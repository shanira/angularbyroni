import { Injectable } from '@angular/core';
import {HttpParams} from '@angular/common/http';
import {Http, Headers} from '@angular/http';

@Injectable()
export class UsersService {
  http:Http;
  getUsers(){
    //get messages from the SLIM rest API (Don't say DB)
    return  this.http.get('http://localhost/angular/slimexampletest/users');
    }

postUser(user){
    let options = {
      headers: new Headers({'content-type': 'application/x-www-form-urlencoded'}
    )};
    var params = new HttpParams().append('name',user.name).append('phone',user.phone);
    return this.http.post('http://localhost/angular/slimexampletest/users',params.toString(), options);     
  }

  deleteUser(id){
    return this.http.delete('http://localhost/angular/slimexampletest/users/'+id); 
  }

  updateUser(id,user){
    let options = {
      headers: new Headers({'content-type': 'application/x-www-form-urlencoded'}
    )};
    var params = new HttpParams().append('name',user.name).append('phone',user.phone);
    return this.http.put('http://localhost/angular/slimexampletest/users/'+id, params.toString(), options);      
  }
      constructor(http:Http) { 
      this.http = http;}
}
