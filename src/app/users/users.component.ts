import { Component, OnInit } from '@angular/core';
import { UsersService } from './users.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  addform = new FormGroup({
    name: new FormControl('',Validators.required),
    phone: new FormControl('',Validators.required)
  });

  updateform = new FormGroup({
    name: new FormControl('',Validators.required),
    phone: new FormControl('',Validators.required)
  });

  showSlim:Boolean = true; //יוזר המאפשר על ידי לחיצה לראות את הנתונים מסלים כאשר הוא שווה לאמת
  users;
  usersKeys = [];
  updates = [];
  lastOpenedToUpdate;//משתנה המשמש לראות איזה עדכון של יוזר מסוים נפתח אחרון

  sendData(){
    if(this.addform.invalid) return;
    this.service.postUser(this.addform.value).subscribe(response =>{
      console.log(response);
      this.service.getUsers().subscribe(response => {
        this.users =  response.json();
        this.usersKeys = Object.keys(this.users);
      });      
    });
  }

  //פונקציה המגדירה מתי להראות את העדכון
  showUpdate(key){
    if(this.updates[key]){//לסגור את העדכון ליוזר
      this.updates[key] = false;
    }else{
      if(this.lastOpenedToUpdate){ //סוגר את העדכון אם למשתמש אחר פתוח
        this.updates[this.lastOpenedToUpdate] = false;
      }
      //מראה את הטופס למשתמש לאחר שלחץ על עדכון
      this.updates[key] = true;
      this.updateform.get('name').setValue(this.users[key].name);
      this.updateform.get('phone').setValue(this.users[key].phone);
      this.lastOpenedToUpdate = key;      
    } 
  }

  //עדכון היוזר
  updateUser(id){
    if(this.updateform.invalid) return;
    this.service.updateUser(id,this.updateform.value).subscribe(response =>{
      console.log(response);
      this.service.getUsers().subscribe(response => {
        this.users =  response.json();
        this.usersKeys = Object.keys(this.users);
      });      
    });    
  }


  constructor(private service:UsersService) {}

  //הצגת רשימת היוזרים
  ngOnInit() {
    this.service.getUsers().subscribe(response => {
      this.users =  response.json();
      this.usersKeys = Object.keys(this.users);
    });
    /*this.service.getMessagesFire().subscribe(fusers =>{
      this.fusers = fusers;
      console.log(this.fusers);
    });    */
  }


}